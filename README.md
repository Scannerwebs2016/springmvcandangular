Code Challenge
==============

## Application
This application has been built basically using Java 8, Maven, Spring MVC and Angular 4.


**Building:**

_mvn clean install_ (including integration tests)


_mvn clean install -DskipTests=true_ (excaping tests)


**Running locally:**

_mvn jetty:run-forked_ (It will start a Jetty server listening at http://localhost:8080/)


**Deploying:**

Once the build is complete, find the _ROOT.war_ under _target_ directory ready to be deployed in any server


## Web REST service
CRUD methods implemented by the REST api service are the following:
 
* Create a new company:

>> _curl -X_ **POST** _http://viabill.mircloud.host_**/rest/company/** _-d "{'name':'My Company','city':'Dublin','address':'Apartment 27, The Schooner','country':'Ireland','owners':['John Doe', 'Will Smith']}" -H "Content-Type: application/json"_


* List all companies:

>> _curl -X_ **GET** _http://viabill.mircloud.host_**/rest/company/**


* Get details about a company with id=537bda1f-f59c-4db0-9fee-02c426ab6d13:

>> _curl -X_ **GET** _http://viabill.mircloud.host_**/rest/company/537bda1f-f59c-4db0-9fee-02c426ab6d13/**


* Update a company:

>> _curl -X_ **PUT** _http://viabill.mircloud.host_**/rest/company/** _-d "{'id': '537bda1f-f59c-4db0-9fee-02c426ab6d13', 'name':'My Company','city':'Dublin 7','address':'Apartment 27, The Schooner','country':'Ireland','owners':['John Doe', 'Will Smith']}" -H "Content-Type: application/json"_


* Add beneficial owner(s) a company with id=537bda1f-f59c-4db0-9fee-02c426ab6d13:

>> _curl -X_ **PUT** _http://viabill.mircloud.host_**/rest/company/537bda1f-f59c-4db0-9fee-02c426ab6d13/** _-d "['Mary Doe']" -H "Content-Type: application/json"_


## JavaScript Client
JS client implemented with Angular 4. 


There is only one view implemented where all the CRUD methods implemented by the REST service are used.

The view can be accessed from the _http://viabill.mircloud.host/_ (port expected by Angular is 80)


## Upload code to a public repository and deploy to a hosted solution
* Link to Bitbucket [repository](https://Scannerwebs2016@bitbucket.org/Scannerwebs2016/springmvcandangular/) 


* Link to index view [http://viabill.mircloud.host/](http://viabill.mircloud.host/) 


* Link to REST entry point [http://viabill.mircloud.host/rest/company/](http://viabill.mircloud.host/rest/company/) 

_The application is hosted by MirHosting Cloud Services, listening in port 80 of a Tomcat Server v9.0.8-jdk-1.8.0_172_

## Considerations
* Authentication proposal:

>> A digest basic authentication mechanism could be implemented easily with Spring Security based on username and password.
It could be stored in a different concurrent map or in a proper data base if we move forward to a proper persistence layer
implementation. Once, the user is authenticated, he or she would be authenticated till http session expiration (in this 
application is 30 minutes). 

>>Any access to views or resources are granted till end of the session. Spring would bring us more advanced features
such as authorization to different resources based on URI, csrf, etc.

* How can you make the service redundant? 

>> Scaling horizontally our architecture with two or more nodes working in parallel and a load balancer (like Ngix) will make the service redundant.

* What considerations should you do?

>> Firstly, the **persistence layer**: the current implementation would not work because the persistence layer has been implemented in memory by concurrent map, which means, each JVM would have a different 'data base', making the app useless. So, the first consideration
is about the persistence layer. It could be implemented by a proper data base, running in a separate server which acts a joint 
point between any nodes in the architecture. In this case any node would have the same data. But it is also possible to use a
data base in memory such as ehCache (for instance, ehCache using Terracota maintains the data synchronized between any instances 
of ehCache running in different JVM's associated to Terracota).

>> Secondly, the **session management**: the session data is stored by the node which responds to the http request, but in a HA architecture (and depending on how a load balancer server is implemented), we can faced the following scenario: two consecutive requests from the same user can hit different nodes, what means the session data would be impossible to be used by the server because might not exist in that node. The solution is to use a session management platform such as REDIS. If not, then we can't rely on session data, so 
a proper micro services rest architecture could work.


## LIVE demo
![Application LIVE demo screenshot](application.JPG)
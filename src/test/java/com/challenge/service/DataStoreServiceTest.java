package com.challenge.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.challenge.model.Company;
import com.google.common.collect.Sets;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(value = "classpath:applicationContext.xml")
public class DataStoreServiceTest {

	@Autowired
	private DataStoreService service;
	
	private Company company;
	
	@Before
	public void setUp() {
		company = new Company("name", "address", "city", "country", Sets.newHashSet("John Doe"));
		assertNull(company.getId());
		company = service.create(company);
	}
	
	@After
	public void tearDown() {
		service.clear();
	}
	
	@Test
	public void testCreate() {		
		Company result = service.create(company);
		assertNotNull(result);		
		assertNotNull(result.getId());
		
		// Clone and original are different objects
		assertFalse(company == result);	
		assertTrue(company.equals(result));
		
		assertNotNull(service.create(new Company(null,null,null,null,null)));
	}
	
	@Test
	public void testFindOne() {
		Company result = service.findOne(company.getId());
		
		// Company found
		assertNotNull(result);
		
		// Clone and original are different objects
		assertFalse(company == result);	
		assertTrue(company.equals(result));
		
		// Company not found
		assertNull(service.findOne("nonExistingId"));
	}
	
	@Test
	public void testFindAll() {
		assertEquals(1, service.findAll().size());		
	}
	
	@Test
	public void testUpdate() {
		Company updated = company.clone();
		
		// Only name and owners are updated
		updated.setName("newName");
		updated.setOwners(Sets.newHashSet("Peter Rabbit"));
		
		Company result = service.update(updated);
		
		// Clone and original are different objects
		assertFalse(updated == result);	
		assertTrue(updated.equals(result));
		
		// Except name and owners, the rest of the fields remain the same
		assertEquals(company.getAddress(), result.getAddress());
		assertEquals(company.getCity(), result.getCity());
		assertEquals(company.getCountry(), result.getCountry());
		assertEquals(company.getEmail(), result.getEmail());
		assertEquals(company.getId(), result.getId());
		assertEquals(company.getPhone(), result.getPhone());
		
		// Check name and owners fields have been updated
		assertNotEquals(company.getName(), result.getName());
		assertNotEquals(company.getOwners(), result.getOwners());
	}
	
	@Test
	public void testUpdate_failedCompanyNotFound() {
		Company updated = company.clone();
		updated.setId("nonExistingId");
		
		assertNull(service.update(updated));
	}
		
}

package com.challenge.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.challenge.model.Company;
import com.challenge.service.DataStoreService;
import com.google.common.collect.Sets;
import com.google.gson.Gson;

/**
 * Integration test for class {@link com.challenge.controller.RestCompanyController}
 * 
 * @author Jorge Marin Guerrero
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(value = "classpath:applicationContext.xml")
public class RestCompanyControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private DataStoreService service;	

	private MockMvc mockMvc;
	private Company company;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		company = new Company("name", "address", "city", "country", Sets.newHashSet("John Doe"));
		company.getOwners().add("John Doe");
		service.create(company);
	}
	
	@After
	public void tearDown() {
		service.clear();
		mockMvc = null;
	}

	@Test
	public void testFindAll() throws Exception {
		String companyId = company.getId();
		String expectedJSON = "[{\"id\":\"" + companyId + "\",\"name\":\"name\",\"address\":\"address\",\"city\":\"city\",\"country\":\"country\",\"owners\":[\"John Doe\"]}]";
		
		mockMvc.perform(MockMvcRequestBuilders.get("/rest/company/"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(expectedJSON));
	}

	@Test
	public void testFindOne_ok() throws Exception {
		String companyId = company.getId();
		String expectedJSON = "{\"id\":\"" + companyId + "\",\"name\":\"name\",\"address\":\"address\",\"city\":\"city\",\"country\":\"country\",\"owners\":[\"John Doe\"]}";
		
		mockMvc.perform(MockMvcRequestBuilders.get("/rest/company/" + companyId ))
		        .andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(expectedJSON));
	}
	
	@Test
	public void testFindOne_notFound() throws Exception {
		String companyId = "nonExistingCompanyId";
		String expectedJSON = "";
		
		mockMvc.perform(MockMvcRequestBuilders.get("/rest/company/" + companyId ))
				.andExpect(status().isNotFound())
				.andExpect(content().string(expectedJSON));
	}
	
	@Test
	public void testCreate_ok() throws Exception {
		String companyId = company.getId();
		String expectedJSON = "{\"id\":\"" + companyId + "\",\"name\":\"name\",\"address\":\"address\",\"city\":\"city\",\"country\":\"country\",\"owners\":[\"John Doe\"]}";
			
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/rest/company/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(company));						
		
        mockMvc.perform(builder)
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(expectedJSON));        
	}
	
	@Test
	public void testCreate_badRequest() throws Exception {
		String companyId = company.getId();
		String expectedJSON = "{\"id\":\"" + companyId + "\",\"address\":\"address\",\"city\":\"city\",\"country\":\"country\",\"owners\":[\"John Doe\"]}";
			
		// Setting company name to null to trigger one validation error
		company.setName(null);
		
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/rest/company/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(company));						
		
        mockMvc.perform(builder)
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(expectedJSON));        
	}
		
	@Test
	public void testUpdate_ok() throws Exception {
		String companyId = company.getId();
		String expectedJSON = "{\"id\":\"" + companyId + "\",\"name\":\"Mary Doe\",\"address\":\"address\",\"city\":\"city\",\"country\":\"Ireland\",\"owners\":[\"John Doe\"]}";
		
		// Update company name and phone
		Company updatedCompany = company.clone();
		updatedCompany.setName("Mary Doe");
		updatedCompany.setCountry("Ireland");
				
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/rest/company/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(updatedCompany));						
		
		String resultJSON = mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn().getResponse().getContentAsString();
		
		assertEquals(expectedJSON, resultJSON);
		
		updatedCompany = new Gson().fromJson(resultJSON, Company.class);
		
		assertNotEquals(company.getName(), updatedCompany.getName());
		assertNotEquals(company.getCountry(), updatedCompany.getCountry());
		
		assertEquals(company.getId(), updatedCompany.getId());
		assertEquals(company.getAddress(), updatedCompany.getAddress());
		assertEquals(company.getCity(), updatedCompany.getCity());
		assertEquals(company.getEmail(), updatedCompany.getEmail());
		assertEquals(company.getOwners(), updatedCompany.getOwners());
		assertEquals(company.getPhone(), updatedCompany.getPhone());
	}
	
	@Test
	public void testUpdate_notFound() throws Exception {
		String companyId = "nonExistingCompanyId";
		String expectedJSON = "";
		
		// Update company id by a non existing one
		Company updatedCompany = company.clone();
		updatedCompany.setId(companyId);
				
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/rest/company/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(updatedCompany));						
		
        mockMvc.perform(builder)
				.andExpect(status().isNotFound())
				.andExpect(content().string(expectedJSON));        
	}
	
	@Test
	public void testUpdate_badRequest() throws Exception {
		String companyId = company.getId();
		String expectedJSON = "{\"id\":\"" + companyId + "\",\"address\":\"address\",\"city\":\"city\",\"country\":\"country\",\"owners\":[\"John Doe\"]}";
		
		// Setting company name to null to trigger one validation error
		Company updatedCompany = company.clone();
		updatedCompany.setName(null);
				
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/rest/company/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(updatedCompany));						
		
        mockMvc.perform(builder)
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(expectedJSON));        
	}
	
	@Test
	public void testAddOwners_ok() throws Exception {
		String companyId = company.getId();
		
		String newOwner = "Mary Doe";
		Set<String> newOwners = Sets.newHashSet(newOwner);
		
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/rest/company/" + companyId)
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(newOwners));	
		
		String resultJSON = mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn().getResponse().getContentAsString();
		
		Company updatedCompany = new Gson().fromJson(resultJSON, Company.class);
		
		// Checks the newOwners is not included in the original company
		assertFalse(company.getOwners().contains(newOwner));
		
		// Checks the newOwners is included in the updated company
		assertTrue(updatedCompany.getOwners().contains(newOwner));
		
		assertTrue(updatedCompany.getOwners().size() == (company.getOwners().size() + 1));
	}
	
	@Test
	public void testAddOwners_notFound() throws Exception {
		String companyId = "nonExistingCompanyId";
		String expectedJSON = "";
		
		String newOwner = "Mary Doe";
		Set<String> newOwners = Sets.newHashSet(newOwner);
		
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/rest/company/" + companyId)
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(newOwners));	
		
		mockMvc.perform(builder)
				.andExpect(status().isNotFound())
				.andExpect(content().string(expectedJSON));
	}
}
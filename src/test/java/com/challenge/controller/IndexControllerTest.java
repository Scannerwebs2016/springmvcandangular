package com.challenge.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Integration test for class {@link com.challenge.controller.IndexController}
 * 
 * @author Jorge Marin Guerrero
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(value = "classpath:applicationContext.xml")
public class IndexControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	
	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}
	
	@After
	public void tearDown() {
		mockMvc = null;
	}

	@Test
	public void testGetIndexPage_of() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.get("/")
				.accept(MediaType.TEXT_HTML))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.view().name("index"));
	}

	@Test
	public void testGetIndexPage_fail() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.get("/nonExistingURI")
				.accept(MediaType.TEXT_HTML))
				.andExpect(status().isNotFound());
	}
	
}
package com.challenge.controller;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.model.Company;
import com.challenge.service.DataStoreService;


/**
 * Rest Controller to implement CRUD actions for Company model
 * 
 * @author Jorge Marin Guerrero
 *
 */
@RestController
@RequestMapping("/rest/company")
public class RestCompanyController {

    private final static Logger logger = LoggerFactory.getLogger(RestCompanyController.class);

      
    @Autowired
    private DataStoreService service;
    
    @Resource(name = "companyValidator")
    private Validator validator;
    
    @InitBinder("company")
    private void initBinder(WebDataBinder binder) {
    	binder.addValidators(validator);
    }
    
    /**
     * Creates a new company
     * @param company
     * @param result
     * @param response
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json")
    public Company create(@RequestBody @Valid Company company, BindingResult result, HttpServletResponse response) {
        if( !result.hasErrors() ) {
        	logger.info("Creating company={}", company);
        	response.setStatus(HttpServletResponse.SC_CREATED);
        	return service.create(company);
        }else {
        	logger.error("Creation failed: {} missing arguments form company {}", result.getErrorCount(), company);
        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        	return company;
        }        
    }
    
    /**
     * Gets a list of all companies
     * @param response
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public List<Company> findAll(HttpServletResponse response) {
        logger.info("Retrieving all companies");
        response.setStatus(HttpServletResponse.SC_OK);
        return service.findAll();
    }
    
    /**
     * Gets the company which id is passed as parameter
     * @param id
     * @param response
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Company find(@PathVariable String id, HttpServletResponse response) {
        Company company = service.findOne(id);
        if( company != null ) {
        	logger.info("Retrieving company with id = {}", id);
        	response.setStatus(HttpServletResponse.SC_OK);
        }else {
        	logger.warn("Company with id={} not found.", id);
        	response.setStatus(HttpServletResponse.SC_NOT_FOUND);        	
        }       
        return company;
    }

    /**
     * Updates the company passed as parameter
     * @param company
     * @param result
     * @param response
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.PUT, produces = "application/json")
    public Company update(@RequestBody @Valid Company company, BindingResult result, HttpServletResponse response) {
        String id = company.getId();    	
        if( !result.hasErrors() ) {
        	company = service.update(company);
        	if( company != null ) {
        		logger.info("Updating company with id={}", id);  
        		response.setStatus(HttpServletResponse.SC_OK);
        	}else {
        		logger.error("Update failed: company with id {} not found", id);
        		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        	}
        }else {
        	logger.error("Update failed: {} missing arguments for company {}", result.getErrorCount(), company);
        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);        	
        }
        return company;
    }
    
    /**
     * Adds a new owners to the company which id is passed as parameter
     * @param id
     * @param owners
     * @param response
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public Company addOwners(@PathVariable String id, @RequestBody Set<String> owners, HttpServletResponse response) {
        Company company = service.findOne(id);
        if( company != null ) {
        	logger.info("Adding owners {} to company with id={}", owners, id);
        	response.setStatus(HttpServletResponse.SC_OK);
        	
        	company.getOwners().addAll(owners);
        	company = service.update(company);        	
        }else {
        	logger.error("Owners additon failed: company with id={} not found", id);
        	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }        
        return company;
    }
    
   
}


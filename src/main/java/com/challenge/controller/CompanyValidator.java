package com.challenge.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.challenge.model.Company;


/**
 * Validator for check all Payment attributes are present 
 * @author jmg
 *
 */
/**
 * Custom company validator to validate the following fields are mandatory:
 * <ul>
 * <li>name</li>
 * <li>address</li>
 * <li>city</li>
 * <li>country</li>
 * <li>owners</li>
 * </ul>
 * @author Jorge Marin Guerrero
 *
 */
@Component("companyValidator")
public class CompanyValidator implements Validator {

	private final static Logger logger = LoggerFactory.getLogger(CompanyValidator.class);
	
	
    public boolean supports(Class<?> clazz) {
    	return Company.class == clazz;
    }

    public void validate(Object target, Errors errors) {
    	logger.debug("Company validation starts ...");
    	if (!errors.hasFieldErrors()) {
    		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "validation.empty", "Can't be empty");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "validation.empty", "Can't be empty");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "validation.empty", "Can't be empty");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country", "validation.empty", "Can't be empty");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "owners", "validation.empty", "Can't be empty");
        }
    	logger.debug("Company validation completed.");
    }
    
}

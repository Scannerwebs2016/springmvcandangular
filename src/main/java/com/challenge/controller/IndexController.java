package com.challenge.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * View controller to serve views located in /WEB-INF/views/ directory
 * 
 * @author Jorge Maring Guerrero
 *
 */
@Controller
@RequestMapping("/")
public class IndexController {

	private final static Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String getIndexPage() {
		logger.info("View index requested");
		return "index";
	}
}
package com.challenge.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.challenge.model.Company;

@Component
public class DataStoreServiceImpl implements DataStoreService{

	private final static Logger logger = LoggerFactory.getLogger(DataStoreServiceImpl.class);

	private ConcurrentHashMap<String, Company> data = new ConcurrentHashMap<String, Company>();	

	
	public Company update(Company updatedCompany) {
		String id = updatedCompany.getId();		
		data.replace(id, updatedCompany);
		return findOne(id);
	}

	
	public Company create(Company company) {
		String id = company.getId();
		if( id == null ) {
			id = UUID.randomUUID().toString();
			company.setId(id);
		}
		
		data.putIfAbsent(id, company);
		return findOne(id);
	}
	
	/*
	// TODO This method could be used instead of having create and update
	public Company merge(Company company) {
		String id = company.getId();	
		
		// Save action
		if( id == null ) {
			id = UUID.randomUUID().toString();
			company.setId(id);
			
			logger.debug("Creating company={}", company);
			data.putIfAbsent(id, company);
		}
		// Update action
		else {
			logger.debug("Updating company={}", company);
			data.replace(id, company);
		}
		
		return findOne(id);
	}*/
	
	public synchronized Company findOne(String id) {
		logger.debug("Finding company with id={}", id);		
		return data.search(1, (key, company) -> {
			if (company.getId().equals(id)) {
				return company.clone();
			}
			return null;
		});
	}	
	
	public synchronized List<Company> findAll() {	
		logger.debug("Finding all companies");
		List<Company> companies = new ArrayList<Company>();
		data.values().stream().forEach(c->companies.add(c.clone()));		
		return companies;
	}

	public void clear() {
		logger.debug("Clearing all companies from datastore");
		data.clear();
	}
}

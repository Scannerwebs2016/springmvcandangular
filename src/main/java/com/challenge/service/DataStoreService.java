package com.challenge.service;

import java.util.List;

import com.challenge.model.Company;

/**
 * Interface to interact with the persistence layer which in this case is implemented
 * in memory by a concurrent map.<br/>
 * 
 * <ul>
 * <li>Any objects returned by the implemented methods are cloned copies</li>
 * <li>All implemented methods are thread-safe to avoid race conditions</li>
 * </ul>
 * 
 * @author Jorge Marin Guerrero
 *
 */
public interface DataStoreService {

	/**
	 * Updates atomically an existing company with the company passed as parameter.
	 * <b>Atomicity is provided by replace method</b>
	 * @param company
	 * @return
	 */
	public Company update(Company company);
	
	/**
	 * Saves atomically a new company with the company passed as parameter.
	 * <b>Atomicity is provided by putIfAbsent method</b>
	 * @param id
	 * @param company
	 * @return
	 */
	public Company create(Company company);
	
	/**
	 * Retrieves a cloned copy of the company object which id is passed as parameter;otherwise, null
	 * @param id
	 * @return
	 */
	public Company findOne(String id);
	
	/**
	 * Retrieves all the companies
	 * @return
	 */
	public List<Company> findAll();
	
	/**
	 * Clears the data dropping all store data
	 */
	public void clear();
}

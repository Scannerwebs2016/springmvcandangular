package com.challenge.model;

import java.util.HashSet;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Model class
 * 
 * @author Jorge Marin Guerrero
 *
 */
@ToString
@EqualsAndHashCode
public class Company implements Cloneable{
	
	@Getter @Setter 
	private String id;
	
	@Getter @Setter 
	private String name;
	
	@Getter @Setter 
	private String address;
	
	@Getter @Setter 
	private String city;
	
	@Getter @Setter 
	private String country;

	@Getter @Setter
	private Set<String> owners = new HashSet<>();

	// Optionals
	@Getter @Setter 
	private String email;
	
	@Getter @Setter 
	private String phone;

	private Company() {}
	

	public Company(String name, String address, String city, String country, Set<String> owners) {
		this.name = name;
		this.address = address;
		this.city = city;
		this.country = country;
		this.owners = owners;
	}
		
	/**
	 * Clone method to retrieve an exact copy of the company
	 */
	public Company clone() {
		Company clone = new Company();
		clone.setAddress(this.getAddress());
		clone.setCity(this.getCity());
		clone.setCountry(this.getCountry());
		clone.setEmail(this.getEmail());
		clone.setId(this.getId());
		clone.setName(this.getName());
		clone.setPhone(this.getPhone());
		
		if( this.getOwners() != null ) {
			clone.getOwners().addAll(this.getOwners());
		}		
		
		return clone;
	}
	
}

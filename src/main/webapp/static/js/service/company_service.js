'use strict';

angular
		.module('myApp')
		.factory(
				'DataStoreService',
				[
						'$http',
						'$q',
						function($http, $q) {
							var REST_SERVICE_URI = window.location + 'rest/company/';
							console.log("REST_SERVICE_URI =" + REST_SERVICE_URI);

							var factory = {
								findAll : findAll,
								findOne : findOne,
								createCompany : createCompany,
								updateCompany : updateCompany,
								updateOwnersCompany : updateOwnersCompany
							};

							return factory;

							function findAll() {
								var deferred = $q.defer();
								$http
										.get(REST_SERVICE_URI)
										.then(
												function(response) {
													deferred
															.resolve(response.data);
												},
												function(errResponse) {
													console
															.error('Error while finding all companies');
													deferred
															.reject(errResponse);
												});
								return deferred.promise;
							}

							function findOne(id) {
								var deferred = $q.defer();
								$http
										.get(REST_SERVICE_URI + id)
										.then(
												function(response) {
													deferred
															.resolve(response.data);
												},
												function(errResponse) {
													console
															.error('Error while finding company with id='
																	+ id);
													deferred
															.reject(errResponse);
												});
								return deferred.promise;
							}

							function createCompany(company) {
								var deferred = $q.defer();
								$http
										.post(REST_SERVICE_URI, company)
										.then(
												function(response) {
													deferred
															.resolve(response.data);
												},
												function(errResponse) {
													console
															.error('Error while creating company');
													deferred
															.reject(errResponse);
												});
								return deferred.promise;
							}

							function updateCompany(company) {
								var deferred = $q.defer();
								$http
										.put(REST_SERVICE_URI, company)
										.then(
												function(response) {
													deferred
															.resolve(response.data);
												},
												function(errResponse) {
													console
															.error('Error while updating company');
													deferred
															.reject(errResponse);
												});
								return deferred.promise;
							}

							function updateOwnersCompany(id, owners) {
								var deferred = $q.defer();
								$http
										.put(REST_SERVICE_URI + id, owners)
										.then(
												function(response) {
													deferred
															.resolve(response.data);
												},
												function(errResponse) {
													console
															.error('Error while adding owners to company with id='
																	+ id);
													deferred
															.reject(errResponse);
												});
								return deferred.promise;
							}

						} ]);

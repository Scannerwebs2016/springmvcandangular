'use strict';

angular
		.module('myApp')
		.controller(
				'RestCompanyController',
				[
						'$scope',
						'DataStoreService',
						function($scope, DataStoreService) {
							var self = this;
							self.company = {
								id : null,
								name : '',
								address : '',
								city : '',
								country : '',
								owners : [],
								email : '',
								phone : ''
							};
							self.companies = [];
							self.found = {
								id : null,
								name : '',
								address : '',
								city : '',
								country : '',
								owners : [],
								email : '',
								phone : ''
							};

							self.submit = submit;
							self.edit = edit;
							self.reset = reset;
							self.findOne = findOne;
							self.findAll = findAll;
							self.addOwners = addOwners;
							self.addedOwners = false;

							findAll();

							function findAll() {
								DataStoreService
										.findAll()
										.then(
												function(d) {
													$scope.companyId = "";
													self.companies = [];
													self.companies = d;
												},
												function(errResponse) {
													console
															.error('Error while fetching Companies');
												});
							}

							function createCompany(company) {
								DataStoreService
										.createCompany(company)
										.then(
												findAll,
												function(errResponse) {
													console
															.error('Error while creating company');
												});
							}

							function updateCompany(company) {
								DataStoreService
										.updateCompany(company)
										.then(
												findAll,
												function(errResponse) {
													console
															.error('Error while updating company');
												});
							}

							function updateOwnersCompany(id, owners) {
								DataStoreService
										.updateOwnersCompany(id, owners)
										.then(
												findAll,
												function(errResponse) {
													console
															.error('Error while adding owners to company');
												});
							}

							function submit() {
								if (self.company.id === null) {
									console.log('Saving New Company',
											self.company);
									createCompany(self.company);
								} else {
									if (self.addedOwners) {
										updateOwnersCompany(self.company.id,
												self.company.owners);
										console
												.log(
														'Added owners to company with id ',
														self.company.id);
										// Reset the added owners flag
										self.addedOwners = false;
									} else {
										updateCompany(self.company);
										console.log('Company updated with id ',
												self.company.id);
									}
								}
								reset();
							}

							function edit(id) {
								self.addedOwners = false;
								console.info('id to be edited', id);
								for (var i = 0; i < self.companies.length; i++) {
									if (self.companies[i].id === id) {
										self.company = angular
												.copy(self.companies[i]);
										break;
									}
								}
							}

							function addOwners(id) {
								self.addedOwners = true;

								console.info('id to add owners', id);
								for (var i = 0; i < self.companies.length; i++) {
									if (self.companies[i].id === id) {
										self.company = angular
												.copy(self.companies[i]);
										break;
									}
								}
							}

							function findOne(id) {
								if (id == null || id === '') {
									return;
								}
								self.found = {
									id : null,
									name : '',
									address : '',
									city : '',
									country : '',
									owners : [],
									email : '',
									phone : ''
								};
								console.info('id to be found', id);

								self.companies = [];
								$scope.searchText = "";

								DataStoreService
										.findOne(id)
										.then(
												function(d) {
													self.found = d;
													self.companies[0] = d;
													console.info('Found', d);
												},
												function(errResponse) {
													console
															.error(
																	'Error while finding company id',
																	id);
												});
							}

							function deleteCompany(id) {
								DataStoreService
										.deleteCompany(id)
										.then(
												findAll,
												function(errResponse) {
													console
															.error('Error while deleting company');
												});
							}

							function reset() {
								self.company = {
									id : null,
									name : '',
									address : '',
									city : '',
									country : '',
									owners : [],
									email : '',
									phone : ''
								};
								$scope.myForm.$setPristine(); //reset Form
							}

						} ]);

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Company Code Challenge</title>
<style>
.formFieldRequired.ng-valid {
	background-color: lightgreen;
}

.formFieldRequired.ng-dirty.ng-invalid-required {
	background-color: #ff4d4d7d;
}

.formFieldRequired.ng-dirty.ng-invalid-minlength {
	background-color: #ffff00a1;
}

.searchbutton {
	padding: 0.45em;
	background: #337ab7;
	color: white;
}

.clear {
	margin-left: -17px;
	color: black;
}

.searchBox {
	width: 25%;
}
</style>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body ng-app="myApp" class="ng-cloak">
	<div class="generic-container"
		ng-controller="RestCompanyController as ctrl">

		<div class="panel panel-default">

			<div class="panel-heading">
				<span class="lead">Company {{!ctrl.company.id ? 'Creation' :
					(ctrl.addedOwners ? 'Add Owners' : 'Update')}} Form </span>
			</div>
			<div class="formcontainer">
				<form ng-submit="ctrl.submit()" name="myForm"
					class="form-horizontal">
					<input type="hidden" ng-model="ctrl.company.id" />

					<div class="row">
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="id">Id</label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.company.id" name="id"
									class="form-control input-sm" readonly />
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="name">Name<span
								style="color: red;">*</span></label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.company.name" name="name"
									class="formFieldRequired form-control input-sm"
									placeholder="Enter company name" required ng-minlength="3"
									ng-readonly="ctrl.addedOwners" />
								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.name.$error.required">This is a
										required field</span> <span ng-show="myForm.name.$error.minlength">Minimum
										length required is 3</span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="address">Address<span
								style="color: red;">*</span></label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.company.address"
									name="address" class="formFieldRequired form-control input-sm"
									placeholder="Enter company address" required ng-minlength="3"
									ng-readonly="ctrl.addedOwners" />

								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.address.$error.required">This is a
										required field</span> <span ng-show="myForm.address.$error.minlength">Minimum
										length required is 3</span>
								</div>
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="city">City<span
								style="color: red;">*</span></label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.company.city" name="city"
									class="formFieldRequired form-control input-sm"
									placeholder="Enter company city" required ng-minlength="3"
									ng-readonly="ctrl.addedOwners" />

								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.city.$error.required">This is a
										required field.</span> <span ng-show="myForm.city.$error.minlength">Minimum
										length required is 3</span>
								</div>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="country">Country<span
								style="color: red;">*</span></label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.company.country"
									name="country" class="formFieldRequired form-control input-sm"
									placeholder="Enter company country" required ng-minlength="3"
									ng-readonly="ctrl.addedOwners" />

								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.country.$error.required">This is a
										required field.</span> <span
										ng-show="myForm.country.$error.minlength">Minimum
										length required is 3</span>
								</div>
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="owners">Owners<span
								style="color: red;">*</span></label>
							<div class="col-md-7">
								<input type="text" ng-list ng-model="ctrl.company.owners"
									name="owners" class="formFieldRequired form-control input-sm"
									placeholder="Enter company owners separated by comma" required
									ng-minlength="3" />

								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.owners.$error.required">This is a
										required field.</span> <span ng-show="myForm.owners.$error.minlength">Minimum
										length required is 3</span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="email">Email</label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.company.email" name="email"
									class="formFieldRequired form-control input-sm"
									placeholder="Enter company email. [This field is validation free]"
									ng-readonly="ctrl.addedOwners" />
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-2 control-lable" for="phone">Phone</label>
							<div class="col-md-7">
								<input type="phone" ng-model="ctrl.company.phone" name="phone"
									class="formFieldRequired form-control input-sm"
									placeholder="Enter company phone. [This field is validation free]"
									ng-readonly="ctrl.addedOwners" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-actions floatRight">
							<input type="submit"
								value="{{!ctrl.company.id ? 'Create' : 'Update'}}"
								class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
							<button type="button" ng-click="ctrl.reset()"
								class="btn btn-secondary btn-sm">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="lead">List of Companies: </span>
			</div>

			<div class="tablecontainer" style="margin: 1em 0em 1em 0em;">
				<input class="searchBox" ng-model="searchText"
					placeholder="Enter text to search"> <a class="clear"
					ng-click="searchText = ''"> <span
					class="glyphicon glyphicon-remove"></span>
				</a>
				<button type="button" ng-click="ctrl.findAll()"
					class="btn btn-primary"
					title="Filter the list of company by any text">
					<i class="fa fa-filter"> Filter</i>
				</button>
			</div>

			<div class="tablecontainer" style="margin: 1em 0em 1em 0em;">
				<input class="searchBox" ng-model="companyId"
					placeholder="Enter company id to search"> <a class="clear"
					ng-click="companyId = ''"> <span
					class="glyphicon glyphicon-remove"></span>
				</a>
				<button type="button" ng-click="ctrl.findOne(companyId)"
					class="btn btn-primary" title="Search a company by id">
					<i class="fa fa-search"> Search</i>
				</button>
			</div>


			<div class="tablecontainer">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Address</th>
							<th>City</th>
							<th>Country</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Owners</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="company in ctrl.companies |  filter:searchText">
							<td><span ng-bind="company.id"></span></td>
							<td><span ng-bind="company.name"></span></td>
							<td><span ng-bind="company.address"></span></td>
							<td><span ng-bind="company.city"></span></td>
							<td><span ng-bind="company.country"></span></td>
							<td><span ng-bind="company.email"></span></td>
							<td><span ng-bind="company.phone"></span></td>
							<td><span ng-bind="company.owners"></span></td>
							<td>
								<button type="button" ng-click="ctrl.edit(company.id)"
									class="btn btn-success custom-width"
									title="Update this company">
									<i class="fa fa-edit"></i>
								</button>
								<button type="button" ng-click="ctrl.addOwners(company.id)"
									class="btn btn-warning custom-width"
									title="Add owners to this company">
									<i class="fa fa-plus"></i><i class="fa fa-user-o"></i>
								</button>
							</td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>

	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
	<script src="<c:url value='/static/js/app.js' />"></script>
	<script src="<c:url value='/static/js/service/company_service.js' />"></script>
	<script
		src="<c:url value='/static/js/controller/company_controller.js' />"></script>
</body>
</html>